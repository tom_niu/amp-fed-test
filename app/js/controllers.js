'use strict';

var ampTestApp = angular.module('ampTestApp', []);

ampTestApp.controller('DesktopCtrl', ['$scope', function($scope) {
    $scope.users = data || {};
    for(var i=0;i<$scope.users.length;i++) {
        $scope.users[i].picture = $scope.users[i].picture.replace("jpg","png");
    }
}]);