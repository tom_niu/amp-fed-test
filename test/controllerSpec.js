'use strict';

describe("AMPTest controllers", function() {
    describe("DesktopCtrl", function () {
        var scope, ctrl, $httpBackend;
        beforeEach(module('ampTestApp'));
        beforeEach(inject(function(_$httpBackend_, $rootScope, $controller) {
            $httpBackend = _$httpBackend;
            scope = $rootScope.$new();
            ctrl = $controller('DesktopCtrl', {$scope: scope});
        }));

        it('should replace jpg with png', function() {
            expect(scope.users[0].slice(-3)).toBe('png');
        });
    })

})